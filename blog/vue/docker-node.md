# 利用 docker 部署前端应用

## 镜像构建

### 减小尺寸

- 选择 alpine 版本
- npm install --production 不安装 devDependencies
- 分阶段构建，build 阶段用 node 镜像，部署用 nginx 镜像

### 构建速度

- 利用 cache，先加入 ADD package.json /app 并 install，如果 package.json 不变则之前的步骤可以缓存

### Dockerfile

```bash
FROM daocloud.io/library/node:12.16.2-alpine as builder
WORKDIR /usr/src/app
ADD package.json /usr/src/app
RUN npm install --registry=https://registry.npm.taobao.org
ADD . /usr/src/app
RUN npm run build

FROM daocloud.io/library/nginx:1.13.0-alpine
COPY --from=builder /usr/src/app/dist/ /usr/share/nginx/html/
```

## HTTPS

#### 利用 letsencrypt 自动生成 https 证书

- index.docker.io/jrcs/letsencrypt-nginx-proxy-companion:latest

配置其他另个容器名称

```
environment:
  NGINX_PROXY_CONTAINER: nginx
  NGINX_DOCKER_GEN_CONTAINER: nginx-gen
```

- daocloud.io/library/nginx:1.13.3-alpine

- index.docker.io/jwilder/docker-gen:latest
- 创建网络

```bash
docker network create web_default
```

完整 compose

```
version: '2'
services:
  nginx:
    image: daocloud.io/library/nginx:1.13.3-alpine
    net: web_default // 指定前面配置的网络
    labels:
      com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy: 'true'
    container_name: nginx
    restart: unless-stopped
    ports:
    - 80:80
    - 443:443
    volumes:
    - /nginx/conf.d:/etc/nginx/conf.d
    - /nginx/vhost.d:/etc/nginx/vhost.d
    - /nginx/html:/usr/share/nginx/html
    - /nginx/certs:/etc/nginx/certs:ro
  nginx-gen:
    image: index.docker.io/jwilder/docker-gen:latest
    net: web_default
    command: -notify-sighup nginx -watch -wait 5s:30s /etc/docker-gen/templates/nginx.tmpl
      /etc/nginx/conf.d/default.conf
    container_name: nginx-gen
    restart: unless-stopped
    volumes:
    - /nginx/conf.d:/etc/nginx/conf.d
    - /nginx/vhost.d:/etc/nginx/vhost.d
    - /nginx/html:/usr/share/nginx/html
    - /nginx/certs:/etc/nginx/certs:ro
    - /var/run/docker.sock:/tmp/docker.sock:ro
    - /nginx/nginx.tmpl:/etc/docker-gen/templates/nginx.tmpl:ro
  nginx-letsencrypt:
    image: index.docker.io/jrcs/letsencrypt-nginx-proxy-companion:latest
    net: web_default
    container_name: nginx-letsencrypt
    restart: unless-stopped
    volumes:
    - /nginx/conf.d:/etc/nginx/conf.d
    - /nginx/vhost.d:/etc/nginx/vhost.d
    - /nginx/html:/usr/share/nginx/html
    - /nginx/certs:/etc/nginx/certs:rw
    - /var/run/docker.sock:/var/run/docker.sock:ro
    environment:
      NGINX_DOCKER_GEN_CONTAINER: nginx-gen
      NGINX_PROXY_CONTAINER: nginx

```

## Web 应用

设置相应的环境变量自动配置 nginx 配置，https 证书

完整 compose

```
h5-520:
  image: daocloud.io/malianghang/h5-520:latest // 利用dockerfile生成的应用
  command: ''
  net: web_default // 加入到同一个网络
  privileged: false
  restart: always
  ports:
  - '80' // nginx 暴露80端口
  environment:
  - VIRTUAL_PORT=80
  - LETSENCRYPT_HOST=520.malianghang.com
  - VIRTUAL_PROTO=https
  - LETSENCRYPT_EMAIL=xupengfei2010@gmail.com
  - VIRTUAL_HOST=520.malianghang.com
```

## CI/CD

### 采用 daocloud

- 创建项目，选择 github 项目
- 根据 github 中 Dockerfile 生成镜像，push 自动构建
- 创建应用，选择镜像
- 写好 compose yaml
- 设置自动发布 镜像构建自动发布最新应用

### 采用 github action

- 在 action 中 npm install 、build
- 将构建好内容推送到自己服务器
- 速度慢，不推荐
