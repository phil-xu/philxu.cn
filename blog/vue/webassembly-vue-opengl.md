# vue/webassembly/opengl

拖拽本地图片到下方（jpeg 2 的 n 次幂）
<WebAssembly></WebAssembly>

## 实现

### Emscripten 安装

#### 本地安装

安装官方文档就可以了

```bash
# Get the emsdk repo
git clone https://github.com/emscripten-core/emsdk.git

# Enter that directory
cd emsdk

# Fetch the latest version of the emsdk (not needed the first time you clone)
git pull

# Download and install the latest SDK tools.
./emsdk install latest # windows: emsdk install latest

# Make the "latest" SDK "active" for the current user. (writes ~/.emscripten file)
./emsdk activate latest # windows: emsdk activate latest

# Activate PATH and other environment variables in the current terminal
source ./emsdk_env.sh # windows: emsdk_env.bat
```

#### 网速问题

如果实在太慢，可以修改 emsdk.py，把里面的包 url 打印处理，自己下载，url 替换掉

#### hello world

```c
// hello_world.c
#include <stdio.h>

int main() {
  printf("hello, world!\n");
  return 0;
}
```

编译成 js 和 wasm

```bash
./emcc hello_world.c -o out.js
```

运行

```bash
node out.js
```

编译成 html js wasm

```bash
./emcc hello_world.c -o index.html
```

用服务器运行，浏览器访问，可以看到将输出放到了一个 textarea 当中

#### Docker（推荐）

遇到运行环境搭建现在首先想到的就是 docker 了，一个命令搞定

```bash
docker run --rm -v `pwd`:`pwd` trzeci/emscripten emcc hello_world.c -o out.js
```

为什么最后才说？知其然知其所以然

只会用不知道为什么出问题都不知道怎么找

使用上越简单的东西越要知道它是怎么做的

### OpenGL 环境

#### 参数

利用 SDL2 port，生成 WEBGL2 代码

导出 js 中用到的函数，要加下划线

```bash
  -s USE_WEBGL2=1 \
  -s USE_SDL=2 \
  -s EXPORTED_FUNCTIONS='["_initializeOpenGL", "_render", "_loadJPEGImage"]' \
```

使用 cwrap 包装，或 ccall 调用时不需要下划线

```js
// Call C from JavaScript
var add = Module.cwrap(
  'add', // name of C function
  'number', // return type
  ['number', 'number']
); // argument types

var result = Module.ccall(
  'add', // name of C function
  'number', // return type
  ['number', 'number'], // argument types
  [10, 20]
); // arguments
```

直接调用使用下划线

```js
Module._add(10, 20);
```

### Emscripten 编译 ES6 模块

#### 参数

- EXPORT_ES6/MODULARIZE 导出 ES6 形式模块
- USE_ES6_IMPORT_META=0 不用 import.meta.url，就算添加了@babel/plugin-syntax-import-meta 插件打包配合 webpack 还是会报错，所以去掉
- TOTAL_MEMORY=134217728 传递数据的堆的大小，导出模块的 HEAP8：Int8Array(134217728)/Int16Array(67108864)

```bash
  -s WASM=1 \
  -s ASSERTIONS=1  \
  -s EXPORT_ES6=1 \
  -s MODULARIZE=1 \
  -s USE_ES6_IMPORT_META=0 \
  -s TOTAL_MEMORY=134217728 \
  -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap"]'
```

### js 调用

```js
import Module from './renderer';
export default {
  async mounted() {
    let canvas = this.$refs.canvas;
    Module({
      canvas: canvas,
    }).then((module) => {
      module.initializeOpenGL(canvas.width, canvas.height);
    });
  },
};
```

### 图片数据传递

#### 获取数据

```js
const files = event.dataTransfer.files;
if (files && files.length === 1 && files[0].type.match('image/jpeg')) {
  const fileReader = new FileReader();
  fileReader.onload = (event) => {
    if (this._loadImage(new Uint8Array(event.target.result))) {
      // this._invalidate();
    }
  };

  fileReader.onerror = () => {
    console.error('Unable to read file ' + file.name + '.');
  };

  fileReader.readAsArrayBuffer(files[0]);
}
```

#### 申请内存

```js
const numBytes = imageData.length * imageData.BYTES_PER_ELEMENT;
const dataPtr = Module._malloc(numBytes);
const dataOnHeap = new Uint8Array(Module.HEAPU8.buffer, dataPtr, numBytes);
dataOnHeap.set(imageData);
```

#### 传递给二进制程序

```js
Module.loadJPEGImage(dataOnHeap.byteOffset, imageData.length);
```

#### 释放

```js
Module._free(dataPtr);
```
