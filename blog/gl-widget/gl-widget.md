---
date: 2020-1-4
tag:
  - gl-widget使用文档
  - gl-widget
  - webgl
author: PhilXu
location: Shanghai  
---

# 开始


## GLWidget背景
一提到WebGL应用，想到的都是展示3D产品，或是3D网页游戏，在普通页面UI上的应用很少，shadertoy上很多酷炫的shader作为UI的一部分应该是很酷的事情，那为什么很少有人尝试呢，除了兼容性的问题，其中很大一部分原因是从零开始搭建WegGL应用过于繁琐，而引用threejs、babylonjs渲染引擎做这样的事情又大材小用。

所以我就想写一款内核极简，又能快速完成搭建shader运行环境的引擎，可以使开发人员专注于shader效果的编写，或是直接移植shadertoy上惊艳的效果。同时，在架构上注重扩展性，通过编写插件，可以扩展成功能更全的引擎。

![build](https://github.com/newbeea/gl-widget/workflows/build/badge.svg)
![NPM](https://img.shields.io/npm/l/@gl-widget/gl-widget)
![npm](https://img.shields.io/npm/dm/@gl-widget/gl-widget)
![jsDelivr hits (npm)](https://img.shields.io/jsdelivr/npm/hm/@gl-widget/gl-widget)


::: run {title: '简单shader', row: false, reverse: true}
```html
<template>
    <div id="gl-widget" ref="glWidget"></div>
</template>
<script>
export default {
    mounted () {
      
      // 利用 id 或 htmlelement 初始化
      var glWidget = new GLWidget.GLWidget({
      element: this.$refs.glWidget
      })


      let shader = {
        fragmentShader: `
          precision highp float;
          uniform vec2 resolution; //外部传进来的unifrom变量
          uniform float     time; //外部传进来的unifrom变量
          void main () {
            vec2 uv = gl_FragCoord.xy/resolution.xy;   
            vec3 col = 0.5 + 0.5*cos(time+uv.xyx+vec3(0,2,4));
            gl_FragColor = vec4(col,1.0);
          }
        `, // 编写fragmentShader，此例用默认vertexShader
        uniforms: {
          resolution:{
            value: glWidget.getSize() // canvas 大小
          },
          time: {
            value: 0
          }
        }, // 上面 fragmentShader 中用到的分辨率、时间变量
      }
      //每帧动画
      function animate() {
        shader.uniforms['time'].value += 0.01 // 设置变量值产生动画效果
      }

      //开始渲染
      glWidget.renderBackground(shader, animate)

    }
  }
</script> 
<style>
#gl-widget {
  height: 100px;
}
#gl-widget canvas {
  border-radius: 10px;
}
</style>
```
:::

::: run {title: 'Banner应用', row: false, reverse: true}
```html
<template>
    <div id="gl-widget" ref="glWidget"></div>
</template>
<script>
export default {
    async mounted () {
      
      // 利用 id 或 htmlelement 初始化
      var glWidget = new GLWidget.GLWidget({
      element: this.$refs.glWidget
      })

      let res = await axios.get('/_glwidget/shader/star.glsl')
      let shader = {
        fragmentShader: res.data, // 编写fragmentShader，此例用默认vertexShader
        uniforms: {
          resolution:{
            value: glWidget.getSize() // canvas 大小
          },
          time: {
            value: 0
          }
        }, // 上面 fragmentShader 中用到的分辨率、时间变量
      }
      //每帧动画
      function animate() {
        shader.uniforms['time'].value += 0.01 // 设置变量值产生动画效果
      }

      //开始渲染
      glWidget.renderBackground(shader, animate)

    }
  }
</script> 
<style>
#gl-widget {
  height: 100px;
}
#gl-widget canvas {
  border-radius: 10px;
}
</style>
```
:::


::: run {title: '图片特效处理', row: false, reverse: true}
```html
<template>
    <div id="gl-widget"></div>
</template>
<script>
export default {
    async mounted () {
      
      // 利用 id 或 htmlelement 初始化
      var glWidget = new GLWidget.GLWidget({
      element: 'gl-widget'
      })

     
      let shader = {
        fragmentShader: `
          precision highp float;
          uniform vec2 resolution; //外部传进来的unifrom变量
          uniform float     time; //外部传进来的unifrom变量
          uniform sampler2D     image; //外部传进来的unifrom变量
          float rand () {
            return fract(sin(time)*1e4);
          }
          void main () {
            vec2 uv = gl_FragCoord.xy/resolution.xy;   
            vec2 uvR = uv;
            vec2 uvB = uv;

            if (sin(time) > -0.2) {
              uvR.x = uv.x * 1.0 - rand() * 0.02 * 0.8;
              uvB.y = uv.y * 1.0 + rand() * 0.02 * 0.8;

              if(uv.y < rand() && uv.y > rand() -0.1)
              {
                uv.x = (uv + 0.02 * rand()).x;
              }
            }

            vec4 c;
            c.r = texture2D(image, uvR).r;
            c.g = texture2D(image, uv).g;
            c.b = texture2D(image, uvB).b;
            c.a = 1.;

            gl_FragColor = c;
          }
        `, // 编写fragmentShader，此例用默认vertexShader
        uniforms: {
          image: {
            value: new GLWidget.Texture('/_glwidget/image/posz.jpg')
          },
          resolution:{
            value: glWidget.getSize() // canvas 大小
          },
          time: {
            value: 0
          }
        }, // 上面 fragmentShader 中用到的分辨率、时间变量
      }
      //每帧动画
      function animate() {
        shader.uniforms['time'].value += 0.01 // 设置变量值产生动画效果
      }

      //开始渲染
      glWidget.renderBackground(shader, animate)

    }
  }
</script> 
<style>
#gl-widget {
  width: 300px;
  height: 300px;
}
#gl-widget canvas {
  border-radius: 10px;
}
</style>
```
:::

::: run {title: '进度条', row: false, reverse: true}
```html
<template>
    <div id="gl-widget" ref="glWidget"></div>
</template>
<script>
export default {
    async mounted () {
      
      // 利用 id 或 htmlelement 初始化
      var glWidget = new GLWidget.GLWidget({
      element: this.$refs.glWidget
      })

      let res = await axios.get('/_glwidget/shader/progress.glsl')
      let shader = {
        fragmentShader: res.data, // 编写fragmentShader，此例用默认vertexShader
        uniforms: {
          resolution:{
            value: glWidget.getSize() // canvas 大小
          },
          time: {
            value: 0
          }
        }, // 上面 fragmentShader 中用到的分辨率、时间变量
      }
      //每帧动画
      function animate() {
        shader.uniforms['time'].value += 0.01 // 设置变量值产生动画效果
      }

      //开始渲染
      glWidget.renderBackground(shader, animate)

    }
  }
</script> 
<style>
#gl-widget {
  height: 15px;
}
#gl-widget canvas {
  border-radius: 10px;
}
</style>
```
:::


::: run {title: 'Loading效果', row: false, reverse: true}
```html
<template>
    <div id="gl-widget" ref="glWidget"></div>
</template>
<script>
export default {
    mounted () {
      
      // 利用 id 或 htmlelement 初始化
      var glWidget = new GLWidget.GLWidget({
        element: this.$refs.glWidget
      })


      let shader = {
        fragmentShader: `
          precision highp float;
          uniform float time;
          uniform vec2 resolution;

          float Cricle(vec2 uv, vec2 center, float radius, float thickness, float seed) {
      
            float dis = distance(center, uv);
            vec2 centerToPixel =  uv -center;
            float angle = atan(centerToPixel.y, centerToPixel.x);
            
            float s1 = sin(angle *5. -time + 512. + seed)*0.006;
            float s2 = sin(angle *2. + time*1.8+ 21.+ seed)*0.008;
            float noise = s1 + s2;
            return 1.- smoothstep(thickness, thickness + 0.013,abs(dis -radius + noise));
              
          }

          void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
            // Normalized pixel coordinates (from 0 to 1)
            vec2 uv = fragCoord/resolution.xy;
            uv.x *= resolution.x/resolution.y;

            // Time varying pixel color
            float r =  Cricle(uv, vec2((resolution.x/resolution.y)*0.5, 0.5), 0.3, 0.01, 42.);
            float g =  Cricle(uv, vec2((resolution.x/resolution.y)*0.5, 0.5), 0.3, 0.01, 18.);
            float b =  Cricle(uv, vec2((resolution.x/resolution.y)*0.5, 0.5), 0.3, 0.01, 71.);
            vec3 col = vec3(r,g,b);
            // Output to screen
            fragColor = vec4(col,1.0);
          }
          void main(void)
          {
            mainImage(gl_FragColor, gl_FragCoord.xy);
          }
        `, // 编写fragmentShader，此例用默认vertexShader
        uniforms: {
          resolution:{
            value: glWidget.getSize() // canvas 大小
          },
          time: {
            value: 0
          }
        }, // 上面 fragmentShader 中用到的分辨率、时间变量
      }
      //每帧动画
      function animate() {
        shader.uniforms['time'].value += 0.05 // 设置变量值产生动画效果
      }

      //开始渲染
      glWidget.renderBackground(shader, animate)

    }
  }
</script> 
<style>
#gl-widget {
  width: 100px;
  height: 100px;
}
#gl-widget canvas {
  border-radius: 10px;
}
</style>
```
:::


::: run {title: '配合插件展示svg图标', row: false, reverse: true, jsLabs: ['https://cdn.jsdelivr.net/npm/@gl-widget/svg-element@1.0.6/dist/index.umd.js']}
```html
<template>

      <div id="gl-widget" ref="glWidget"></div>
</template>
<script>
export default {
    async mounted () {
      // 获取svg dom
      let svgString = `
      <svg t="1587465515561" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2744" xmlns:xlink="http://www.w3.org/1999/xlink" width="128" height="128"><defs><style type="text/css"></style></defs><path d="M864 64H160C107 64 64 107 64 160v704c0 53 43 96 96 96h704c53 0 96-43 96-96V160c0-53-43-96-96-96zM618.6 831.4c-16.8 3-23-7.4-23-16 0-10.8 0.4-66 0.4-110.6 0-31.2-10.4-51-22.6-61.4 74-8.2 152-18.4 152-146.2 0-36.4-13-54.6-34.2-78 3.4-8.6 14.8-44-3.4-90-27.8-8.6-91.4 35.8-91.4 35.8-26.4-7.4-55-11.2-83.2-11.2-28.2 0-56.8 3.8-83.2 11.2 0 0-63.6-44.4-91.4-35.8-18.2 45.8-7 81.2-3.4 90-21.2 23.4-31.2 41.6-31.2 78 0 127.2 74.6 138 148.6 146.2-9.6 8.6-18.2 23.4-21.2 44.6-19 8.6-67.6 23.4-96.6-27.8-18.2-31.6-51-34.2-51-34.2-32.4-0.4-2.2 20.4-2.2 20.4 21.6 10 36.8 48.4 36.8 48.4 19.4 59.4 112.2 39.4 112.2 39.4 0 27.8 0.4 73 0.4 81.2 0 8.6-6 19-23 16-132-44.2-224.4-169.8-224.4-316.6 0-183.6 140.4-323 324-323S840 331.2 840 514.8c0.2 146.8-89.4 272.6-221.4 316.6z m-196.2-122.2c-3.8 0.8-7.4-0.8-7.8-3.4-0.4-3 2.2-5.6 6-6.4 3.8-0.4 7.4 1.2 7.8 3.8 0.6 2.6-2 5.2-6 6z m-19-1.8c0 2.6-3 4.8-7 4.8-4.4 0.4-7.4-1.8-7.4-4.8 0-2.6 3-4.8 7-4.8 3.8-0.4 7.4 1.8 7.4 4.8z m-27.4-2.2c-0.8 2.6-4.8 3.8-8.2 2.6-3.8-0.8-6.4-3.8-5.6-6.4 0.8-2.6 4.8-3.8 8.2-3 4 1.2 6.6 4.2 5.6 6.8z m-24.6-10.8c-1.8 2.2-5.6 1.8-8.6-1.2-3-2.6-3.8-6.4-1.8-8.2 1.8-2.2 5.6-1.8 8.6 1.2 2.6 2.6 3.6 6.6 1.8 8.2z m-18.2-18.2c-1.8 1.2-5.2 0-7.4-3s-2.2-6.4 0-7.8c2.2-1.8 5.6-0.4 7.4 2.6 2.2 3 2.2 6.6 0 8.2z m-13-19.4c-1.8 1.8-4.8 0.8-7-1.2-2.2-2.6-2.6-5.6-0.8-7 1.8-1.8 4.8-0.8 7 1.2 2.2 2.6 2.6 5.6 0.8 7z m-13.4-14.8c-0.8 1.8-3.4 2.2-5.6 0.8-2.6-1.2-3.8-3.4-3-5.2 0.8-1.2 3-1.8 5.6-0.8 2.6 1.4 3.8 3.6 3 5.2z" fill="" p-id="2745"></path></svg>
      `
      let node = document.createElement("div");
      node.innerHTML = svgString;
      let svgDom = node.querySelector('svg')

      // 利用 id 或 htmlelement 初始化
      var glWidget = new GLWidget.GLWidget({
        element: this.$refs.glWidget // 'gl-widget'
      })

      let res = await axios.get('/_glwidget/shader/bubbles.glsl')
      let shader = {
        fragmentShader: res.data, // 编写fragmentShader，此例用默认vertexShader
        uniforms: {
          resolution:{
            value: glWidget.getSize() // canvas 大小
          },
          time: {
            value: 0
          }
        }, // 上面 fragmentShader 中用到的分辨率、时间变量
        side: GLWidget.RenderSide.DOUBLE
      }


            
      let svg = new SvgElement(shader, svgDom, {
        scale: 4
      })  

      //每帧动画
      function animate() {
        shader.uniforms['time'].value += 0.01 // 设置变量值产生动画效果
      }

      //开始渲染
      glWidget.add(svg)
      glWidget.render(animate)

    }
  }
</script> 
<style>
#gl-widget {
  width: 100px;
  height: 100px;
}
</style>
```
:::


::: run {title: 'Skybox全景 + 相机控制插件', row: false, reverse: true, jsLabs: ['https://cdn.jsdelivr.net/npm/@gl-widget/orbit-controls@1.0.6/dist/index.umd.js']}
```html
<template>
    
  <div>
    <div id="gl-widget"></div>
    <div>鼠标在画布内拖拽</div>
  </div>
</template>
<script>
export default {
    async mounted () {
      
      // 利用 id 或 htmlelement 初始化
      var glWidget = new GLWidget.GLWidget({
      element: 'gl-widget'
      })

     

      
      let imagenames = ['posx', 'negx', 'posy', 'negy', 'posz', 'negz']
      let images = imagenames.map((i) => `/_glwidget/image/${i}.jpg`)

      let sky = new GLWidget.Skybox({
        uniforms: {
          cube: {
            value: new GLWidget.Texture(images)
          }
        }
      })
      glWidget.add(sky)


      let controls = new OrbitControls(glWidget)
      controls.dampingFactor = 0.2

      function animate() {
        controls.update()
      }


      //开始渲染
      glWidget.render(animate)

    }
  }
</script> 
<style>
#gl-widget {
  width: 300px;
  height: 300px;
}
</style>
```
:::


## 安装
### npm
```bash
npm install @gl-widget/gl-widget
```
使用
```js
import { GLWidget, RenderableElement, Texture } from '@gl-widget/gl-widget'
let glWidget = new GLWidget({
  element: "xxx"
})
```
or
```js
import * as GLWidget from '@gl-widget/gl-widget'
let glWidget = new GLWidget.GLWidget({
  element: "xxx"
})
```
or
```js
let GLWidget =  require('@gl-widget/gl-widget')
let glWidget = new GLWidget.GLWidget({
  element: "xxx"
})
```

### script
```html
<script src="https://cdn.jsdelivr.net/npm/@gl-widget/gl-widget/dist/index.umd.js"></script>
```
使用

```js
let glWidget = new GLWidget.GLWidget({
  element: "xxx"
})
```
### Example
下面就是简单的shader动画
```html
<script>
  // 利用 id 或 htmlelement 初始化
  var glWidget = new GLWidget.GLWidget({
  element: 'gl-widget'
  })


  let shader = {
    fragmentShader: `
      precision mediump float;
      uniform vec2 resolution; //外部传进来的unifrom变量
      uniform float     time; //外部传进来的unifrom变量
      void main () {
        vec2 uv = gl_FragCoord.xy/resolution.xy;   
        vec3 col = 0.5 + 0.5*cos(time+uv.xyx+vec3(0,2,4));
        gl_FragColor = vec4(col,1.0);
      }
    `, // 编写fragmentShader，此例用默认vertexShader
    uniforms: {
      resolution:{
        value: glWidget.getSize() // canvas 大小
      },
      time: {
        value: 0
      }
    }, // 上面 fragmentShader 中用到的分辨率、时间变量
  }
  //每帧动画
  function animate() {
    shader.uniforms['time'].value += 0.01 // 设置变量值产生动画效果
  }

  //开始渲染
  glWidget.renderBackground(shader, animate)

</script>
```


::: run {title: '简单shader', row: false, reverse: true}
```html
<template>
    <div id="gl-widget"></div>
</template>
<script>
export default {
    mounted () {
      
      // 利用 id 或 htmlelement 初始化
      var glWidget = new GLWidget.GLWidget({
      element: 'gl-widget'
      })


      let shader = {
        fragmentShader: `
          precision highp float;
          uniform vec2 resolution; //外部传进来的unifrom变量
          uniform float     time; //外部传进来的unifrom变量
          void main () {
            vec2 uv = gl_FragCoord.xy/resolution.xy;   
            vec3 col = 0.5 + 0.5*cos(time+uv.xyx+vec3(0,2,4));
            gl_FragColor = vec4(col,1.0);
          }
        `, // 编写fragmentShader，此例用默认vertexShader
        uniforms: {
          resolution:{
            value: glWidget.getSize() // canvas 大小
          },
          time: {
            value: 0
          }
        }, // 上面 fragmentShader 中用到的分辨率、时间变量
      }
      //每帧动画
      function animate() {
        shader.uniforms['time'].value += 0.01 // 设置变量值产生动画效果
      }

      //开始渲染
      glWidget.renderBackground(shader, animate)

    }
  }
</script> 
<style>
</style>
```
:::

## 功能
### 渲染
#### 渲染背景
```js
glWidget.renderBackground({
  fragmentShader: '',
  uniforms: {
    xxx: {
      value: xxx
    }
  }
}, animation)
```
#### 其他渲染方式
```js
let element = new RenderableElement(shader, geometry) // geometry 参见Geometry 和 BufferGeometry
// or
// let element = new Background(shader)
glWidget.add(element)
glWidget.render(animation)
// or
// glWidget.renderFrame() // 渲染单帧
```
#### 可渲染元素
##### Background
平铺整个canvas背景，不进行矩阵变换
##### Skybox
天空盒
##### RenderableElement
前景元素，需搭配Geometry使用
##### Points
粒子元素，需搭配Geometry使用，渲染Geometry中点


### 相机
可以在渲染时指定其他相机
#### 透视相机
```js
glWidget.render(animation, new PerspectiveCamera(fov, aspect, near, far))
```
#### 正交相机
```js
glWidget.render(animation, new OrthographicCamera(left, right, top, bottom, near, far))
```
相机的控制可以安装@gl-widget/orbit-controls插件，具体使用参见相关文档


### 后期效果
搭配Pass使用, 参考@gl-widget/pass 插件中RenderPass， SavePass
```js
  let renderPass = new RenderPass()
  glWidget.addPass(renderPass)
  let savePass = new SavePass()
  glWidget.addPass(savePass)

  glWidget.renderPass(animate);
```
本例展示使用，无添加任何效果


### 纹理
支持普通纹理、cube纹理


### 其他
#### 常用数学库
Matrix/Quaternion/Vector 等
#### uniform赋值上传
直接修改传入的shader.uniforms, 根据类型自动上传数据到webgl
#### 缓存
根据需要编译、更新


### 插件
#### @gl-widget/obj-parser
解析obj生成Geometry
#### @gl-widget/physical-shader
pbr的光照着色shader
#### @gl-widget/basic-geometry
球形等基础几何体
#### @gl-widget/orbit-controls
相机控制插件：拖拽旋转、滚轮缩放
#### @gl-widget/svg-element
根据svg生成RenderableElement
#### @gl-widget/font-element
根据文字生成RenderableElement


