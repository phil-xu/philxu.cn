---
home: true
heroText: GLWidget
tagline: 面向UI 微内核 易扩展的WebGL框架
actionText: 快速上手 →
actionLink: /gl-widget/gl-widget/
features:
  - title: 简洁至上
    details: 微内核，14kb min+gzip
  - title: 易扩展
    details: 插件方式扩展功能，提供官方插件
  - title: 高性能
    details: 必要时编译shader，更新uniform
footer: 黑ICP备20001731号 | Copyright © 2018-present Phil Xu
---
