module.exports = {
  title: 'Phil Xu',
  description: 'webgl vue threejs developer',
  head: [
    // ico
    ["link", {rel: "icon", href: `/favicon.ico`}],
    // meta
    ["meta", {name: "robots", content: "all"}],
    ["meta", {name: "author", content: "phil xu"}],
    ["meta", {name: "keywords", content: "webgl vue 前端 glsl shader glwidget"}],
    ["meta", {name: "apple-mobile-web-app-capable", content: "yes"}],
    // baidu statstic
    // ["script", {src: "https://hm.baidu.com/hm.js?xxxxxxxxxxx"}]
  ],
  plugins: {
    '@vuepress/back-to-top': {},
    '@vuepress/medium-zoom': {
      selector: '.theme-default-content img',
      options: {
        margin: 16
      }
    },
    'sitemap': {
      hostname: 'https://www.philxu.cn'
    },
    'vuepress-plugin-baidu-autopush': {    
    },
    'coderunner': {
      'jsLabs': ['https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js', 'https://cdn.jsdelivr.net/npm/@gl-widget/gl-widget@1.0.14/dist/index.umd.js']
    }, 
    'vuepress-plugin-auto-sidebar': {
      titleMap: {
    		"projects": "项目",
    		"gl-widget": "GLWidget",
    		"gl-widget-tech": "GLWidget相关技术",
        "page-builder": "Page Builder",
    		"vue": "VUE相关"
      },
      // sidebarDepth: 5
    }
  },
  theme: 'theme', // OR shortcut: @vuepress/blog
  themeConfig: {
    nav: [
      {
        text: 'GLWidget',
        link: '/gl-widget/gl-widget'
      },
      {
        text: 'GLWidget相关技术',
        link: '/gl-widget-tech/rollup'
      },
      {
        text: 'Page Builder',
        link: '/page-builder/page-builder'
      },
      {
        text: '其他项目',
        link: '/projects/2016-5-6-postcard'
      },
      {
        text: 'VUE',
        link: '/vue/'
      },
      {
        text: '关于',
        link: '/aboutme/',
      },
      {
        text: 'github',
        link: 'https://github.com/newbeea',
      },
    ]
  },
}
