---
date: 2019-6-26
tag:
  - about me
author: PhilXu
location: Shanghai
---

# 简历

## 联系方式

- 手机：15900836787
- Email：xupengfei2010@gmail.com

## 个人信息

- 许鹏飞/男/1989
- 本科/浙江大学计算机系数字媒体技术
- 研究生/浙江大学计算机系计算机应用技术 CAD实验室
- 工作年限：6 年
- 个人网站：<https://www.philxu.cn>

## 技能清单
- 前端开发：Vue/Javascript/Typescript/HTML/CSS/SASS
- 后端开发：Nodejs/Graphql/Midwayjs/AWS相关
- 3D 开发：WebGL/Threejs/GLSL/Babylonjs
- 工程化工具：Webpack/Rollup/Gulp/Vite/Grunt
- 数据库相关：MySQL/Typeorm
- 版本管理、自动化部署工具：Git/Gitlab CI/Jenkins/Docker
- 云和开放平台：微信开放平台/淘宝开放平台
- 移动端：Android/iOS/Flutter
## 工作经历
### 杭州图为科技有限公司 （ 2020 年 6 月 ~ 至今 ）
Web技术经理、项目经理，主要负责跨境礼服定制品牌的web相关项目建设，进度把控，人员培养及团队建设
#### Fameandpartners项目
##### 项目概述
主打礼服品类定制的跨境品牌，项目包含在线定制商店前后端，礼服3D动态效果展示，以及对应中后台管理系统
##### 责任描述
负责前后端架构，使用vue+midwayjs组合的前后端框架，统一使用typescript开发，以微前端方式多技术体系并存，逐步替换老旧功能模块，以解决上线时间紧，相关开发人员web相关开发经验较少的问题。数据部分使用typeorm+mysql，建模满足复杂定制选项与互斥组合的需求，使前后端能够轻松拓展定制功能。搭建graphql服务，搭配codegenerator前后端统一类型，无缝融合成熟跨境服务shopify的相关功能，redis缓存搭配DataLoader，无需手写大量loader解决N+1问题。搭建CI/CD服务，制定开发发布流程制度。搭建日志管理系统、异常监控系统，搭配钉钉提供服务监控相关能力。同时负责相关功能的主力开发、项目管理、人员培养、团队建设等工作。项目在入职后两个月正式上线，并保持1-2周迭代的更新频率，团队开发效率显著提升。

#### 图为科技中台项目
##### 项目概述
图为科技中台项目，融合定制生产、产品管理、布料管理，App后台管理，3D项目管理等功能的系统
##### 责任描述
基于各个项目特点与未来潜在目标用户，搭建基于RBAC的权限控制模块，满足复杂的组织权限，支持前端路由级别、按钮级别、后端api级别、graphql field级别的权限控制。负责产品管理、订单管理、数据分析等功能的技术方案及主力开发。


### 上海笔尔工业设计有限公司 （ 2015 年 4 月 ~ 2020年5月 ）

联合创始人、产品技术负责人，主要负责项目管理、WebGL 在线定制、前端交互、子系统开发，主导完成珠宝首饰领域 C2M 在线定制系统。

#### 馬良行珠宝首饰在线CAD定制系统

##### 项目概述
面向珠宝首饰领域的在线 3D 定制与3D打印对接系统，用户可对定制内容、截面、尺寸、宽度、材质、工艺、镶嵌、镭射等多个方面进行定制，程序根据定制参数实时反馈定制的 3D 效果，后台系统还原定制内容，导出 stl 文件，直接进行 3D 打印。

##### 责任描述
利用threejs 3D引擎完成所有相关功能开发，其中针对自定义镶嵌的复杂交互，采用参数化建模的方式使镶嵌自动吸附排列。开发 shader 表现多种材质、工艺、实时展现声波分色戒指动态效果，支持语音、文字、画板、指纹采集等多种交互定制，实时生成3D模型预览，极大的提高用户定制的体验。利用Nodejs实现前后端一致的3D建模逻辑，完成用户自主定制下单到工厂3D打印生产发货的直连。在国内webgl应用尚未兴起的阶段投入商用，并受邀在手淘App实现了3D定制体验。作为项目负责人，主导以此为技术核心，与官网、订单管理子系统组成的系统项目，获得浦东信息化委员会百万元资金支持，赢得多项创业比赛。

##### 项目效果
<img src="../_images/about/ring.jpg" class="zoom" width = "32%" height = "auto" style="display: inline;"/>
<img src="../_images/about/diamond.jpg" width = "32%" height = "auto" style="display: inline;"/>
<img src="../_images/about/process.jpg" width = "32%" height = "auto" style="display: inline;"/>

<https://www.philxu.cn/projects/2019-6-14-big-screen.html>

#### 馬良行在线商城

##### 项目概述

馬良行在线商城，包含常规电商浏览购物功能，创新的 3D 在线定制模块，网站采用 spa 方式构建，app 式体验。

##### 责任描述

负责在线商城前后端开发，商城项目采用angularjs的前端框架，后端leancloud的serverless服务。实现3D定制模块在商城中的应用，解决webgl体验问题，在早期手机端webgl兼容不好时期搭建后台渲染功能，使用360图片模拟3D效果。利用 docker 快速开发部署的特点，进行微信开发者模式后台、微信 JSSDK 鉴权录音等微服务的开发。产品后台管理部分利用vue特性，根据定制内容抽象语音、文字、地图等定制模块，后台拖拽生成定制页面。

##### 项目效果
<img src="../_images/about/detail.jpg" width = "29%" height = "auto" style="display: inline;"/>
<img src="../_images/about/customize.jpg" width = "29%" height = "auto" style="display: inline;"/>
<img src="../_images/about/dna.jpg" width = "12%" height = "auto" style="display: inline;"/>
<img src="../_images/about/sound.jpg" width = "12%" height = "auto" style="display: inline;"/>
<img src="../_images/about/ones.jpg" width = "12%" height = "auto" style="display: inline;"/>

<https://www.philxu.cn/projects/2017-12-31-cad.html>


#### 馬良行智能制造系统
##### 项目概述

系统自动汇总官网、京东、淘宝、天猫定制订单，根据产品工艺、订单交付时间、工厂产能等因素智能分配工厂，跟踪、改变每笔订单状态，获取订单信息，查看订单 3D 模型，对订单进行修改、加急、报错等操作，导出订单报表等功能。

##### 责任描述
根据不同平台开放api开发订单汇总模块。根据定制产品订单以及3D打印产品的特殊性，设计开发订单跟踪系统。使用vue+cordova打包的形式，实现工厂跟踪App，提供实时查看订单信息、3D效果，NFC写入、发货等功能，降低工厂生产错误率，提高生产效率。

### 阿里云 （ 2013 年 11 月 ~ 2014 年 12 月 ）

#### 帝喾云渲染

##### 项目概述

云渲染是阿里云云计算应用的尝试之一，主要针对电影动漫产业中的渲染部分，利用阿里云的服务器集群完成每帧渲染。

##### 责任描述

主要负责接到调度任务后的渲染部分，还有相关运维工具搭建，利用 python 完成渲染模块，在完成本职工作基础上，参与到用 c++ 开发的调度任务当中。工作期间完成对《昆塔之盒子总动员》动画电影渲染的支持，在图形学专业知识外，快速熟悉云计算相关知识并投入到云计算相关开发。

## 其他项目

#### RightMap

##### 项目概述

利用 threejs 构建 web 端 3D 地图 SDK，应用在3D机房可视化、消防演习等领域。

##### 责任描述

独立完成交互设计、SDK 接口设计，项目基于json生成地图，利用多种 svg 标签生成 3D 模型，展示室内地图，提供多种相机操作的接口以及自由的事件处理、信息提示接口等自定义地图需要的功能。在各种实际应用中找到性能瓶颈不断提升加载性能。

## 开源项目：
#### 拖拽式、插件化的Lowcode页面生成系统（vue3+vite+midwayjs+typescript）

同时为运营人员和开发者服务的lowcode页面生成器，拖拽式布局，快捷样式设置。
插件化设计，支持第三方组件免编译安装、第三方配置面板。
支持导出vue组件代码、支持保存模板组件、生成html。

<img src="../../images/74e9fc1d83ebb6ce5cd73392699036c79b6d63e57781a26adcc64d35ffe4896e.png" width = "32%" height = "auto" style="display: inline;"/>
<img src="../../images/7fdef53ebc409dfc853940d4992b31d583a08a2aa273a31a93f04e20b2a1045c.png" width = "32%" height = "auto" style="display: inline;"/>
<img src="../../images/848ad6c14ea8ab28406682c4d7bc763a3f17cccbbd8a6a4edab198e9e0f1ab54.png" width = "32%" height = "auto" style="display: inline;"/> 

演示：<https://www.philxu.cn/page-builder/page-builder.html>
源码：<https://github.com/newbeea/page-builder>

#### 微内核、易扩展的WebGL框架（webgl+glsl+typescript）
采用微内核架构、插件化的方式实现的 webgl 框架，面向UI shader特效。
通过插件系统可以扩展成功能更全的 webgl 引擎，提供pbr、相机、obj解析等官方插件。
演示：<https://www.philxu.cn/gl-widget/gl-widget.html>
源码：<https://github.com/newbeea/gl-widget>

